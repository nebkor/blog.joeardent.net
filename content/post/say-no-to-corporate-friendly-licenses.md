+++
author = "Joe Ardent"
date = 2017-01-17
description = ""
draft = false
slug = "say-no-to-corporate-friendly-licenses"
title = "Say no to corporate-friendly licenses"

categories = [
    "politics",
    "copyright"
]

tags = [
    "lucasfilm",
    "alembic",
    "gpl",
    "philosophy",
    "anti-capitalism",
    "old blog"
]

keywords = [ "" ]
+++

*In which I commit to anti-capitalism*

I want to start this with a little story about working at Industrial Light & Magic. For those not in the know, ILM is the visual effects studio that George Lucas started in order to make his indie space-adventure movie, _Star Wars_, a tale of a man who became absolutely corrupted by his power and was, thankfully, eventually destroyed. When the movies came out, they absolutely captured the imagination of nearly an entire generation, at which point, George Lucas held onto that imagination as a cultural hostage[^1] [^2], and demanded payment from anyone else who foolishly tried to play with the storyblocks Lucas had made. This is all to say that the Empire that Lucas eventually built was very, very much into copyright maximalism.

That was all way before my time there, of course; I was there for only a few years, from 2008 to 2011. But while I was there, I was lucky enough to be one of the main developers of an [Open Source project for efficiently sharing animated geometric data in an application-agnostic way, called Alembic](http://www.alembic.io/). The inside story of how Alembic came to be is a fairly sordid one, but that's for another time; suffice to say it was an unlikely thing that we did, but we pushed on and wound up changing the entire industry.

Even though ILM had some [prior experiencing with open-sourcing some of their software](http://www.openexr.com/), we faced an up-hill battle internally. Like I said, Lucas' Empire was far more comfortable in the role of cultural vampire, and their legal team had far more experience in taking rather than giving.  Their head of counsel, an IP lawyer named Jennifer Siebley, was particularly inimical to the GPL, so much as to have what I would term an allergy to it.  For example, we were not allowed to include the source for zlib in our project, because the zlib project bundled some third-party code that was licensed under the GPL that linked against zlib. To the ILM legal team, any code that was distributed along with GPL'd code was tainted, even though zlib itself was not GPL'd. This is not a proper interpretation of the GPL, but as my manager said at the time, "It's her job to say no."[^3]

*ANYWAY*, at the time, this left a fairly bad taste in my mouth regarding the GPL. It was viral! It made it impossible for businesses to adopt your code! Indeed, our BSD-licensed project crucially relied on corporate network effects; the more studios adopted Alembic-based workflows, the better we and everyone else would be, because it enabled studios to share assets in a way that was far easier than before.[^4] Had it been GPL'd, its studio adoption would have been far more anemic, and it probably would have turned into yet another geometry format that died on the vine.

Which brings us to the present. I've lately been thinking about all that time and this stuff, and I've come to the following conclusion: fuck all capitalist vampires. We live in a world where [a group of eight people have control of more economic means than the poorest four billion people](https://www.theguardian.com/global-development/2017/jan/16/worlds-eight-richest-people-have-same-wealth-as-poorest-50), and the power disparity between our corporate masters and regular humans is unimaginably vast. There's very little that you or I can do, but we do have one ace up our sleeves: we write software, and software increases our leverage.  So don't give that leverage to the leviathans trying to commoditize you.

Now, I realize that we are living in a material world, and we are a material girl, so I'm not relying solely on philosophical appeals. Consider the case that you wish to profit from the fruits of your keyboard. If you release your software as GPL, you are then free at any time to release new versions that are closed-source, while all your competitors would have to grovel before you in order to procure a commercial license (or you could give your friends licenses for free; the point is, that's your call). This is a pretty compelling commercial case for the GPL, and it's what, eg, Sleepycat (BerkeleyDB) or Trolltech (Qt) did before they were bought by Oracle and Nokia, respectively.

But for me, though, a big part of my motivation to commit to using the GPL is so that people like Jennifer Siebley will tell their corporate overlords like ILM[^5] to not use my software. I'm a little vindictive like that.

So here's my plea to you, fellow software developers. When you write software for free, make it awesome, and make it GPL'd. Make it so good that others must choose between using the best shit, or further filling the corporate coffers of those who wish to enslave us.

[^1]: This is the cultural equivalent of [giving new mothers free infant formula until they stop lactating, and then charging them for it going forward.](http://www.businessinsider.com/nestles-infant-formula-scandal-2012-6?op=1)

[^2]: In case anyone was confused about who had the right to control how his story was told, try to find a legal copy of the original Star Wars trilogy that's not all crapped up with Old Man Lucas smell.

[^3]: I pointed out that it's not, in fact, her job to say no; it's her job to use her training and intellect to correctly divine what the law and license allows, but I lost that battle, as you might imagine. It was frustrating to be in the thrall of a lawyer who didn't understand the difference between patenting and copyrighting, but there you go; that was pretty much the Lucasfilm corporate culture in a nutshell.

[^4]: The business case for sharing the code, of course, was to enable studios to outsource work to cheaper studios in places like Macedonia. Yay capitalism!

[^5]: Lucasfilm, ILM's parent company, is now owned by Disney, who also own Pixar and Marvel.

---
By the way, I couldn't figure out an elegant way to fit this into the main body of this post, but I hope it's super fucking obvious that George Lucas' wealth is the Dark Side of the Force, and he is Vader. Which is double-tragic, because George purposely made Vader a tragic figure, and who wants to become a living tragedy, especially when you spelled it out so plainly yourself what it means to be evil?
