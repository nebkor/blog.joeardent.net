+++
author = "Joe Ardent"
date = 2018-04-17
description = ""
draft = true
slug = "pipedream-an-incomplete-literature-review"
title = "Pipedream: an incomplete review of the literature"

categories = [
    "software"
]

tags = [
    "design",
    "pipedream"
]

keywords = [ "" ]
+++

### *In which I prepare you for disappointment*

First off, this is not going to be a comprehensive articulation of the titular matter; I've been trying to think of a way to write that, and have concluded that it's pretty much not going to happen any time real soon, but what I can do is provide a sketch and a series of supporting vignettes.  This is meant to be a sketch of the outline of the vision, so I can point to something to show others, or use it to orient myself while trying to figure out what to do.

Secondly, Pipedream, as the name suggests, doesn't yet exist, and part of its nature is that it's hard to describe without a concrete reference to something like it. Imagine trying to describe fire to someone who's never seen or felt a flame, and that's similar task.

But enough prevarication, and on with the sketch!

### *But first, a rant*
