+++
author = "Joe Ardent"
date = 2016-02-16
description = ""
draft = false
slug = "minor-thoughts-on-a-major-thing"
title = "Minor thoughts on a major thing"

categories = [ "personal" ]

tags = [
    "recurse center",
    "pipedream",
    "old blog"
]

keywords = [ "" ]
+++

Last week, I started my three-month "batch" at the [Recurse Center](https://recurse.com/), which is described as a "tuition-free self-directed educational retreat for programmers."  The point of it is to get better at programming computers, by spending your days hanging out with 50 or so other people who are there for the same reason.  Attending RC is something I've wanted to do for several years, but until very recently, I never thought I'd be able to (due to time and money, mostly, but I suddenly found myself with a ton of the former and a mild surplus of the latter).  Needless to say, I was extremely excited when I was accepted, but after I was accepted (which happened late in December), most of my time and energy were consumed with converting a school bus into a mobile house so that we could drive it from California to New Jersey, and live in it while I attend the Recurse Center.  That project is a whole 'nother thing about which I'll write more later.

Anyway, my experience so far, after only a week, has been extremely positive.  Everyone here is incredibly kind, helpful, enthusiastic, and generous.  I'm not surprised at that, as the Recurse Center has been very [intentional about creating a safe, pro-social space](https://www.recurse.com/manual), which is one of the things that attracted me to it in the first place.  I'm looking forward to eleven more weeks of getting to know my batch-mates, working on interesting things, and meeting the new batch in five more weeks (a new three-month batch starts every six weeks, so half the people here are from my batch, and half are from the previous batch).

Before I got here, I wanted to work on creating a new programming language and environment for programming.  This is about as ambitious as it sounds, and is way beyond my current abilities, but I wanted something that would stretch me.  After a day or two of reading blogs and trying to set up various toolchains to get started on this project, I realized that I was so far out of my depth that I was floundering.  I'd not written a line of code, and already felt like I was wasting my time and opportunity here.  So on the third day, I decided to dive into my pre-existing plan-Bs: work through the [Nand to Tetris](http://www.nand2tetris.org/) book, and through [The Structure and Interpretation of Computer Programs](http://sarabander.github.io/sicp/html/index.xhtml#SEC_Contents) book.  The former is a from "silicon" up comprehensive course on how [Von Neumann](https://en.wikipedia.org/wiki/Von_Neumann_architecture) machines work (you're working with simulated hardware, but starting with Nand gates and ending with writing a video game that runs on the computer/OS you made), while the latter is a [revered modern classic](http://www.amazon.com/review/R403HR4VL71K8) on the "science" in "computer science" as it applies to software.  Between the two of them, one would have a pretty competent grasp on what computer programming really is, top to bottom, as they approach the subject from different directions and meet in the middle.

Luckily, I'm not alone in having the impulse to go through these texts, so there are groups of about a dozen people each (with some overlap) doing it with me.  We've organized into meeting two days/week for an hour or two to talk about the material, go through the exercises, and generally help each other out.  I'm loving it.

Right now, my plan is to spend the first six weeks or so doing these projects, and the last six weeks tilting at my new programming language windmill, using the knowledge and techniques from the first six weeks' work.  But who knows; something may come up that grabs my attention more compellingly, and that's OK.  There are so many cool things that people here are doing, and everyone is so interesting.

More later, friends and unmet-friends.
